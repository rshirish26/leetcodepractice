package com.leetcode.shirish.LeetCodePractice.august;

public class SingleNumber {

	/*
	 * Given a non-empty array of integers, every element appears twice except for
	 * one. Find that single one.
	 * 
	 * Note:
	 * 
	 * Your algorithm should have a linear runtime complexity. Could you implement
	 * it without using extra memory?
	 * 
	 * Example 1:
	 * 
	 * Input: [2,2,1] Output: 1 Example 2:
	 * 
	 * Input: [4,1,2,1,2] Output: 4
	 */

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int[] input = { 4, 1, 2, 1, 2 };
		int output = singleNumber(input);
	}

	public static int singleNumber(int[] nums) {
		
		/*2

		A little more information on XOR operation.

		XOR a number with itself odd number of times the result is number itself.
		XOR a number even number of times with itself, the result is 0.
		Also XOR with 0 is always the number itself.
*/
		int x = 0;
		for (int a : nums) {
			x = x ^ a;
			System.out.println(x);
			
		}
		return x;
		
		

	}

}
