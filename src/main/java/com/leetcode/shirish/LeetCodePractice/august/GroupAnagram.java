package com.leetcode.shirish.LeetCodePractice.august;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*Given an array of strings, group anagrams together.

Example:

Input: ["eat", "tea", "tan", "ate", "nat", "bat"],
Output:
[
  ["ate","eat","tea"],
  ["nat","tan"],
  ["bat"]
]
Note:

All inputs will be in lowercase.
The order of your output does not matter.*/

public class GroupAnagram {

	public static void main(String[] args) {
		String [] input = {"eat", "tea", "tan", "ate", "nat", "bat"};
		List<List<String>> result = groupAnagrams(input);
		System.out.println("Result : "+result);

	}

	public static List<List<String>> groupAnagrams(String[] strs) {
		//hashMap of String and List
		//interate the array
		//sort the word in ascending order
		//store the sorted word as key in hashmap and corresponding word as List
		//before inserting a new word just check that it si present in hashmap or nit
		List<List<String>>  output = new ArrayList<List<String>>();
		 Map<String,List<String>>  result = new HashMap<>();
		 for(String word : strs)
		 {
			 char [] wordAarry = word.toCharArray();
			 Arrays.sort(wordAarry);
             String sortedWord = new String(wordAarry);
			 if(result.containsKey(sortedWord))
			 {
				 result.get(sortedWord).add(word);
				 continue;
			 }
			 result.put(sortedWord, new ArrayList<>(Arrays.asList(word)));
		 }

		return new ArrayList<List<String>>(result.values());

	}

}
