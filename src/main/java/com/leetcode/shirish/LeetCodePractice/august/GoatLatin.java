package com.leetcode.shirish.LeetCodePractice.august;

import java.util.ArrayList;
import java.util.List;

/*A sentence S is given, composed of words separated by spaces. Each word consists of lowercase and uppercase letters only.

We would like to convert the sentence to "Goat Latin" (a made-up language similar to Pig Latin.)

The rules of Goat Latin are as follows:

If a word begins with a vowel (a, e, i, o, or u), append "ma" to the end of the word.
For example, the word 'apple' becomes 'applema'.
 
If a word begins with a consonant (i.e. not a vowel), remove the first letter and append it to the end, then add "ma".
For example, the word "goat" becomes "oatgma".
 
Add one letter 'a' to the end of each word per its word index in the sentence, starting with 1.
For example, the first word gets "a" added to the end, the second word gets "aa" added to the end and so on.
Return the final sentence representing the conversion from S to Goat Latin. 

 

Example 1:

Input: "I speak Goat Latin"
Output: "Imaa peaksmaaa oatGmaaaa atinLmaaaaa"
Example 2:

Input: "The quick brown fox jumped over the lazy dog"
Output: "heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa"
         heTmaa uickqmaaa rownbmaaaa oxfmaaaaa umpedjmaaaaaa overmaaaaaaa hetmaaaaaaaa azylmaaaaaaaaa ogdmaaaaaaaaaa
 

Notes:

S contains only uppercase, lowercase and spaces. Exactly one space between each word.
1 <= S.length <= 150.*/

public class GoatLatin {
	

	public static void main(String[] args) {
       String input = "The quick brown fox jumped over the lazy dog";
       String result = toGoatLatin(input);
       System.out.println("result :" +result);
	}

	public static String toGoatLatin(String input) {
		
		//Break down the sentence into words
		//interate through the words
		//check first letter of the word
		//if vowel append ma + index
		//if consonent trip get first then append it at the last add ma and get nuber of a
		//add in the list
		List<String> result = new ArrayList<String>();
		String [] words = input.split(" ");
		for(int i=0; i< words.length; i++)
		{
			StringBuffer s = new StringBuffer(words[i]);
			char firstChar = s.charAt(0);
			if(isVowel(firstChar))
			{
				s.append("ma");
				String defaultString = getNumberOfA(i);
				s.append(defaultString);
			}
			if(isConsonent(firstChar))
			{
			    s.deleteCharAt(0);
			    s.append(firstChar);
			    s.append("ma");
			    String defaultString = getNumberOfA(i);
			    s.append(defaultString);			    
			}
			result.add(s.toString());
			
		}
		
		
		
		return String.join(" ", result);

	}

	private static String getNumberOfA(int index) {
		StringBuffer s= new StringBuffer();
		for(int i=0 ;i<=index;i++)
		{
			s.append("a");
		}
		return s.toString();
	}

	private static boolean isConsonent(char ch) {
		if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U'  )
            return false;
        else
		    return true;
	}

	private static boolean isVowel(char ch) {
		if(ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u' || ch == 'A' || ch == 'E' || ch == 'I' || ch == 'O' || ch == 'U'  )
            return true;
        else
		return false;
	}

}
