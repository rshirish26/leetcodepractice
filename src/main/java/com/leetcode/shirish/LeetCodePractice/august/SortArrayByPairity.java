package com.leetcode.shirish.LeetCodePractice.august;

import java.util.Arrays;

public class SortArrayByPairity {

	/*
	 * Given an array A of non-negative integers, return an array consisting of all
	 * the even elements of A, followed by all the odd elements of A.
	 * 
	 * You may return any answer array that satisfies this condition.
	 * 
	 * 
	 * 
	 * Example 1:
	 * 
	 * Input: [3,1,2,4] Output: [2,4,3,1] The outputs [4,2,3,1], [2,4,1,3], and
	 * [4,2,1,3] would also be accepted.
	 * 
	 * 
	 * Note:
	 * 
	 * 1 <= A.length <= 5000 0 <= A[i] <= 5000
	 */
	public static void main(String[] args) {
		int [] a = {3,1,2,4};
		
		System.out.println("Result :" +Arrays.toString(sortArrayByParity(a)));
	}

	public static int[] sortArrayByParity(int[] input) {

				int start = 0;
				int end = input.length-1;
		
				while (start < end) {
					if(input[start] % 2 == 0)
					{
						start++;
					} else {
						if(input[end]%2==0)
						{
							int temp = input[end];
							input[end] = input[start];
							input[start] = temp;
						}
						end--;
					}
					
				}
				return input;
		
				

	}

}
