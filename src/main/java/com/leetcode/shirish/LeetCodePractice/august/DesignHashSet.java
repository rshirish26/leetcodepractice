package com.leetcode.shirish.LeetCodePractice.august;

public class DesignHashSet {

	/*
	 * Design a HashSet without using any built-in hash table libraries.
	 * 
	 * To be specific, your design should include these functions:
	 * 
	 * add(value): Insert a value into the HashSet. contains(value) : Return whether
	 * the value exists in the HashSet or not. remove(value): Remove a value in the
	 * HashSet. If the value does not exist in the HashSet, do nothing.
	 * 
	 * Example:
	 * 
	 * MyHashSet hashSet = new MyHashSet(); hashSet.add(1); hashSet.add(2);
	 * hashSet.contains(1); // returns true hashSet.contains(3); // returns false
	 * (not found) hashSet.add(2); hashSet.contains(2); // returns true
	 * hashSet.remove(2); hashSet.contains(2); // returns false (already removed)
	 * 
	 * Note:
	 * 
	 * All values will be in the range of [0, 1000000]. The number of operations
	 * will be in the range of [1, 10000]. Please do not use the built-in HashSet
	 * library.
	 */

	private int bucket = 1000;
	private int itemsInBucket = 1001;;
	private boolean[][] table;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/** Initialize your data structure here. */
	public DesignHashSet() {
		table = new boolean[bucket][itemsInBucket];

	}

	public void add(int key) {
		int hashCode = hash(key);
		int position = position(key);
		table[hashCode][position] = true;
	}

	private int position(int key) {

		return key / bucket;
	}

	private int hash(int key) {

		return key % bucket;
	}

	public void remove(int key) {
		int hashCode = hash(key);
		int position = position(key);
		table[hashCode][position] = false;
	}

	/** Returns true if this set contains the specified element */
	public boolean contains(int key) {
		int hashCode = hash(key);
		int position = position(key);
		return table[hashCode][position] == true;

	}

}
