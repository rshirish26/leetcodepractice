package com.leetcode.shirish.LeetCodePractice.august;

import java.util.HashMap;
import java.util.Map;

public class WordDictionary {

	/*
	 * You should design a data structure that supports adding new words and finding
	 * if a string matches any previously added string.
	 * 
	 * Implement the WordDictionary class:
	 * 
	 * WordDictionary() Initializes the object. void addWord(word) adds word to the
	 * data structure, it can be matched later. bool search(word) returns true if
	 * there is any string in the data structure that matches word or false
	 * otherwise. word may contain dots '.' where dots can be matched with any
	 * letter.
	 * 	
	 * 
	 * Example:
	 * 
	 * Input
	 * ["WordDictionary","addWord","addWord","addWord","search","search","search",
	 * "search"] [[],["bad"],["dad"],["mad"],["pad"],["bad"],[".ad"],["b.."]] Output
	 * [null,null,null,null,false,true,true,true]
	 * 
	 * Explanation WordDictionary wordDictionary = new WordDictionary();
	 * wordDictionary.addWord("bad"); wordDictionary.addWord("dad");
	 * wordDictionary.addWord("mad"); wordDictionary.search("pad"); // return False
	 * wordDictionary.search("bad"); // return True wordDictionary.search(".ad"); //
	 * return True wordDictionary.search("b.."); // return True
	 * 
	 * 
	 * Constraints:
	 * 
	 * 1 <= word.length <= 500 word in addWord consists lower-case English letters.
	 * word in search consist of '.' or lower-case English letters. At most 50000
	 * calls will be made to addWord and search . Hide Hint #1 You should be
	 * familiar with how a Trie works. If not, please work on this problem:
	 * Implement Trie (Prefix Tree) first.
	 */

	TrieNode root = null;
	char endPoint;

	public static void main(String[] args) {
		WordDictionary ws = new WordDictionary();
		ws.addWord("a");
		ws.addWord("ab");
		boolean result = ws.search("..");
		System.out.println("Rsult :" + result);

	}

	public WordDictionary() {
		root = new TrieNode();
		endPoint = '*';
	}

	public void addWord(String wordL) {
		String word = wordL.toLowerCase();
		for (int i = 0; i < word.length(); i++) {
			createPrefixTrie(i, word);
		}

	}

	private void createPrefixTrie(int i, String word) {
		TrieNode node = root;
		for (int j = i; j < word.length(); j++) {
			if (!node.children.containsKey(word.charAt(j))) {
				TrieNode newNode = new TrieNode();
				node.children.put(word.charAt(j), newNode);

			}
			node = node.children.get(word.charAt(j));
		}
		node.children.put(endPoint, null);
	}

	public boolean search(String wordL) {
		TrieNode node = root;
		String word = wordL.toLowerCase();
		int count = ( word.split("\\.", -1).length ) - 1;
		System.out.println(count);
		if(word.length() == count)
			return false;
		System.out.println("value SHIRISH  =" + word);
		for (int i = 0; i < word.length(); i++) {
			System.out.println("value here  =" + word.charAt(i));
			if (!node.children.containsKey(word.charAt(i)) && (word.charAt(i) != '.')) {
				return false;
			}
			if (word.charAt(i) == '.') {
				for (Map.Entry<Character, TrieNode> child : node.children.entrySet()) {
					if (child.getValue() == null) {
						node = child.getValue();
						break;
					}
					if ((i + 1) < word.length() && word.charAt(i + 1) == '.') {
						node = child.getValue();
						continue;
					}
					if ((child.getValue() != null && child.getValue().children.containsKey('*'))
							|| ((i + 1) < word.length() && child.getValue().children.containsKey(word.charAt(i + 1)))) {
						node = child.getValue();
						break;
					}
				}
				continue;

			}
			node = node.children.get(word.charAt(i));
		}
		return node == null ? false : node.children.containsKey(endPoint) ? true : false;

	}

	public static class TrieNode {
		Map<Character, TrieNode> children = new HashMap<Character, TrieNode>();
	}
}
