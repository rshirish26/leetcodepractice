package com.leetcode.shirish.LeetCodePractice.august;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FindDuplicates {

	/*
	 * Given an array of integers, 1 ≤ a[i] ≤ n (n = size of array), some elements
	 * appear twice and others appear once.
	 * 
	 * Find all the elements that appear twice in this array.
	 * 
	 * Could you do it without extra space and in O(n) runtime?
	 * 
	 * Example:
	 * 
	 * Input: [4,3,2,7,8,2,3,1]
	 * 
	 * Output: [2,3]
	 */

	public static void main(String[] args) {
        int [] nums = {4,3,2,7,8,2,3,1};
        List<Integer> result = findDuplicates(nums);
        System.out.println("Result : "+Arrays.asList(result) );
	}

	public static List<Integer> findDuplicates(int[] nums) {
			List<Integer> result = new ArrayList<Integer>();
			Map<Integer, Boolean> tracker = new HashMap<Integer, Boolean>();
			
			for(int i=0; i< nums.length; i++)
			{
				if(tracker.containsKey(nums[i]) && tracker.get(nums[i])==true)
				{
					result.add(nums[i]);
				} else 
				tracker.put(nums[i], true);
			}
			return result;
	
		}

}
