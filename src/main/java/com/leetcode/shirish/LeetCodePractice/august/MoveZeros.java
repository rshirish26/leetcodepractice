package com.leetcode.shirish.LeetCodePractice.august;

public class MoveZeros {
	
/*	Given an array nums, write a function to move all 0's to the end of it while maintaining the relative order of the non-zero elements.

	Example:

	Input: [0,1,0,3,12]
	Output: [1,3,12,0,0]
	Note:

	You must do this in-place without making a copy of the array.
	Minimize the total number of operations.
*/
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	 public static void moveZeroes(int[] nums) {
	        if (nums.length == 0 || nums == null) return;
	        int idx = 0;
	        for (int i = 0; i < nums.length; i++) {
	            if (nums[i] != 0) {
	                nums[idx++] = nums[i];
	            }
	        }
	        while (idx < nums.length) {
	            nums[idx++] = 0;
	        }
	        return;
	    }

}
