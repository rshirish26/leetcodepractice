package com.leetcode.shirish.LeetCodePractice.august;

public class MaxSubArray {
	
/*	Given an integer array nums, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum.

			Example:

			Input: [-2,1,-3,4,-1,2,1,-5,4],
			Output: 6
			Explanation: [4,-1,2,1] has the largest sum = 6.
			Follow up:

			If you have figured out the O(n) solution, try coding another solution using the divide and conquer approach, which is more subtle.*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int [] input = {-2,1,-3,4,-1,2,1,-5,4};
		int result = maxSubArray(input); 
		System.out.println("Result :" +result);

	}
	
	
	public static int maxSubArray(int[] input) {
		int maxSoFar= input[0];
		int maxEndingHere = input[0];
		
		for(int i=1; i< input.length; i++)
		{
			int current = input[i];
			maxEndingHere = Math.max(current, maxEndingHere +current);
			maxSoFar = Math.max(maxSoFar, maxEndingHere);
		}
		
		
		return maxSoFar;

	}

}
