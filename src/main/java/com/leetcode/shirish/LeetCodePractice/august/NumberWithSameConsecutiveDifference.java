package com.leetcode.shirish.LeetCodePractice.august;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NumberWithSameConsecutiveDifference {

	/*
	 * Return all non-negative integers of length N such that the absolute
	 * difference between every two consecutive digits is K.
	 * 
	 * Note that every number in the answer must not have leading zeros except for
	 * the number 0 itself. For example, 01 has one leading zero and is invalid, but
	 * 0 is valid.
	 * 
	 * You may return the answer in any order.
	 * 
	 * 
	 * 
	 * Example 1:
	 * 
	 * Input: N = 3, K = 7 Output: [181,292,707,818,929]
	 * 
	 * 
	 * Explanation: Note that 070 is not a valid number, because it has leading
	 * zeroes. Example 2:
	 * 
	 * Input: N = 2, K = 1 Output:
	 * [10,12,21,23,32,34,43,45,54,56,65,67,76,78,87,89,98]
	 * 
	 * 
	 * Note:
	 * 
	 * 1 <= N <= 9 0 <= K <= 9
	 */

	// PseudoCode
	/*      
	  
	  */

	public static void main(String[] args) {
		int[] result = numsSameConsecDiff(9, 0);
		int min = (int) Math.pow(10, 3);
		int max = (int) Math.pow(10, 3 + 1) - 1;
		System.out.println("values " + min + " " + max);
		System.out.println("values " + Arrays.toString(result));

	}

	public static int[] numsSameConsecDiff(int N, int K) {
		// take care of the case when n=1
		List<Integer> result = new ArrayList<Integer>();
//		if (N == 1 && K > 1) {
//			return new int[1];
//		}
		if (N == 1 && K <= 9) {
			return new int[] { 0,1, 2, 3, 4, 5, 6, 7, 8, 9 };
		}
		for (int i = 1; i <= 9; i++) {
			logic(i, N, K, result, 1);
		}
		Collections.sort(result);
		return result.stream().distinct().mapToInt(e -> e).toArray();
	}

	private static void logic(int number, int n, int k, List<Integer> result, int loopCounter) {
		int max = (int) Math.pow(10, n) - 1;
		if (number >= max) {
			return;
		}

		while (loopCounter < n) {
			if ((number % 10 + k) >= 0 && (number % 10 + k) <= 9) {
				logic(number * 10 + (number % 10 + k), n, k, result, loopCounter);
				addNumber(number * 10 + (number % 10 + k), result, n);

			}
			if ((number % 10 - k) >= 0 && (number % 10 - k) <= 9) {
				logic(number * 10 + (number % 10 - k), n, k, result, loopCounter);
				addNumber(number * 10 + (number % 10 - k), result, n);

			}
			loopCounter++;
			break;
		}

	}

	private static void addNumber(int number, List<Integer> result, int n) {
		int min = (int) Math.pow(10, n - 1);
		int max = (int) Math.pow(10, n) - 1;
		if (number >= min && number <= max) {
			result.add(number);
		}
	}

}
