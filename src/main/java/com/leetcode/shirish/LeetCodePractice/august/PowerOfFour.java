package com.leetcode.shirish.LeetCodePractice.august;

public class PowerOfFour {

	/*
	 * Given an integer (signed 32 bits), write a function to check whether it is a
	 * power of 4.
	 * 
	 * Example 1:
	 * 
	 * Input: 16 Output: true Example 2:
	 * 
	 * Input: 5 Output: false Follow up: Could you solve it without loops/recursion?
	 */

	public static void main(String[] args) {
        int number = 625;
        boolean result = isPowerOfFour(number);
        System.out.println("Result : "+Math.pow(2, 4));
        
	}

	public static boolean isPowerOfFour(int num) {
			if(num==0) return false;
			 
			   int pow = (int) (Math.log(num) / Math.log(4));
			   if(num==Math.pow(4, pow)){
			       return true;
			   }else{
			       return false;
			   }
		

	}

}
