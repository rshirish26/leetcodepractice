package com.leetcode.shirish.LeetCodePractice.august;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class LongestPalindrome {

	/*
	 * Input: "abccccdd"
	 * 
	 * Output: 7
	 * 
	 * Explanation: One longest palindrome that can be built is "dccaccd", whose
	 * length is 7.
	 */

	public static void main(String[] args) {
		String input = "abccccdd";
		int result = longestPalindrome(input);
		System.out.println("Value here is " + result);

	}

	public static int longestPalindrome(String input) {
		// convert the string into array of character
		// get the distinct values in an List<Character>
		// Find the count of each character
		// if even add it into the final count
		// if odd add count -1 in the final count
		// if odd present the add one
		long plalindromeCount = 0;
		List<Character> list = input.chars().mapToObj(mapper -> (char) mapper).collect(Collectors.toList());

		Map<Character, Long> frequencyMap = list.stream()
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
		int oddCount = 0;
		for (Map.Entry<Character, Long> entry : frequencyMap.entrySet()) {
			if (entry.getValue() % 2 == 0) {
				plalindromeCount = plalindromeCount + entry.getValue();
			} else {
				oddCount++;
				plalindromeCount = plalindromeCount + (entry.getValue() - 1);
			}
		}
		if (oddCount > 0) {
			plalindromeCount = plalindromeCount + 1;
		}

		return (int) plalindromeCount;
	}
}
