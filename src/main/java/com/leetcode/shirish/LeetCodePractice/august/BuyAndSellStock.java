package com.leetcode.shirish.LeetCodePractice.august;

import java.util.Arrays;

public class BuyAndSellStock {
	/*
	 * Say you have an array for which the ith element is the price of a given stock
	 * on day i.
	 * 
	 * Design an algorithm to find the maximum profit. You may complete at most two
	 * transactions.
	 * 
	 * Note: You may not engage in multiple transactions at the same time (i.e., you
	 * must sell the stock before you buy again).
	 * 
	 * Example 1:
	 * 
	 * Input: [3,3,5,0,0,3,1,4] Output: 6 Explanation: Buy on day 4 (price = 0) and
	 * sell on day 6 (price = 3), profit = 3-0 = 3. Then buy on day 7 (price = 1)
	 * and sell on day 8 (price = 4), profit = 4-1 = 3. Example 2:
	 * 
	 * Input: [1,2,3,4,5] Output: 4 Explanation: Buy on day 1 (price = 1) and sell
	 * on day 5 (price = 5), profit = 5-1 = 4. Note that you cannot buy on day 1,
	 * buy on day 2 and sell them later, as you are engaging multiple transactions
	 * at the same time. You must sell before buying again. Example 3:
	 * 
	 * Input: [7,6,4,3,1] Output: 0 Explanation: In this case, no transaction is
	 * done, i.e. max profit = 0.
	 */

	public static void main(String[] args) {
		int [] prices = {1,2,4,2,5,7,2,4,9,0};
		int result = maxProfit(prices);
		System.out.println("Result " + result);

	}

	public static int maxProfit(int[] prices) {
		// [3,3,5,0,0,3,1,4]
		 int fb = Integer.MIN_VALUE;
		 int sb = Integer.MIN_VALUE;
		 int fs = 0;
		 int ss = 0;
		 
		 for (int i = 0; i < prices.length; i++) {
			 
			 
			 fb = Math.max(fb, -prices[i]);   //-1 => -1 => -1
			 fs = Math.max(fs, fb+prices[i]); //0  => 1  => 3
			 sb = Math.max(sb, fs-prices[i]); //-1  => -1 => -1
			 ss=  Math.max(ss, sb+prices[i]); //0	=> 1  => 3	 
		 }
		return ss;
		// firstBuy =  -3
		// firstSell = fb + current 
		// secondBuy = fs - current
		// secondSell = sb + current
		
	
		
/*		// int min = 0;
		int max = 0;
		int[] profit = new int[100];
		int counter = 0;

		// [3,3,5,0,0,3,1,4]
		// {1,4,2};
		if(prices.length < 2)
			return 0;
		int pointer = prices[0];

		for (int i = 1; i < prices.length; i++) {
			if (prices[i] >= prices[i - 1]) {
				max = prices[i];	
			} else {
				profit[counter] = max - pointer;
				System.out.println("Value here :"+max +" " +pointer+" "+profit[counter] );
				pointer = prices[i];
				max =0;
				counter++;
			}
			if(i == prices.length -1 && max !=0)
			{
				profit[counter] = max - pointer;
			}		
		}
		Arrays.sort(profit);
		if(profit.length < 2)
		{
			return 0;
		}
		
		return profit[profit.length-1]+profit[profit.length-2];*/

	}

}
