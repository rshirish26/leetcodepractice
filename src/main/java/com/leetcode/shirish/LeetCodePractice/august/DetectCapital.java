package com.leetcode.shirish.LeetCodePractice.august;

public class DetectCapital {

	/*
	 * Given a word, you need to judge whether the usage of capitals in it is right
	 * or not.
	 * 
	 * We define the usage of capitals in a word to be right when one of the
	 * following cases holds:
	 * 
	 * All letters in this word are capitals, like "USA". All letters in this word
	 * are not capitals, like "leetcode". Only the first letter in this word is
	 * capital, like "Google". Otherwise, we define that this word doesn't use
	 * capitals in a right way.
	 * 
	 * 
	 * Example 1:
	 * 
	 * Input: "USA" Output: True
	 * 
	 * 
	 * Example 2:
	 * 
	 * Input: "FlaG" Output: False
	 * 
	 * 
	 * Note: The input will be a non-empty word consisting of uppercase and
	 * lowercase latin letters.
	 * 
	 * ASCII value of uppercase alphabets – 65 to 90. ASCII value of lowercase
	 * alphabets – 97 to 122.
	 */

	public static void main(String[] args) {
		String word = "Hello";
		boolean result =detectCapitalUse(word);
		System.out.println("Result :   " +result);

	}

	public static boolean detectCapitalUse(String word) {

		// check if all the words are small (small true )or capital (capital true)
		// starting from index 1
		// if smlll true then (then either case first element is okay)
		// if capital then first element should be capital
			boolean small = true;
			boolean capital = true;
			if (word == null || word.length() == 0)
				return false;
	
			String wordWithoutFirstLetter = word.substring(1);
	
			for (int e : wordWithoutFirstLetter.toCharArray()) {
				if (e < 65 || e > 90) {
					capital = false;
				}
				if (e < 97 || e > 122) {
					small = false;
				}
			}
			int firstChar = word.charAt(0);
	
			if (small == true && firstCharIsCapitalOrSmall(firstChar)) {
				return true;
			}
			if (capital == true && firstCharIsCapital(firstChar)) {
				return true;
			}
			return false;
	
		}
	
		private static boolean firstCharIsCapital(int firstChar) {
			if (firstChar >= 65 && firstChar <= 90) {
				return true;
			}
			return false;
		}
	
		private static boolean firstCharIsCapitalOrSmall(int firstChar) {
			if (firstChar >= 65 && firstChar <= 90 || firstChar >= 97 && firstChar <= 122) {
				return true;
			}
			return false;
		}

}
