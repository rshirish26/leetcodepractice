package com.leetcode.shirish.LeetCodePractice.august;

public class ValidPalindrome {

	/*
	 * Given a string, determine if it is a palindrome, considering only
	 * alphanumeric characters and ignoring cases.
	 * 
	 * Note: For the purpose of this problem, we define empty string as valid
	 * palindrome.
	 * 
	 * Example 1:
	 * 
	 * Input: "A man, a plan, a canal: Panama" Output: true Example 2:
	 * 
	 * Input: "race a car" Output: false
	 * 
	 * 
	 * Constraints:
	 * 
	 * s consists only of printable ASCII characters.
	 */
	
	
	//######################################################
	
	
/*	class Solution {
	    public boolean isPalindrome(String s) {
	        
	        int low = 0;
	        int high = s.length() - 1;
	        
	        while(low < high) {
	            while(low < s.length() && !Character.isLetterOrDigit(s.charAt(low))) {
	                low++;
	            }
	            if(low > s.length()) {
	                return true;
	            }
	            while(high >= 0 && !Character.isLetterOrDigit(s.charAt(high))) {
	                high--;
	            }
	            if(high < 0) {
	                return true;
	            }
	            if(Character.toLowerCase(s.charAt(low++)) != Character.toLowerCase(s.charAt(high--))) {
	                return false;
	            }
	        }
	        
	        return true;
	    }
	}
*/
	public static void main(String[] args) {
		
		String input = "A man, a plan, a canal: Panama";
		boolean result =  isPalindrome(input);
		System.out.println("Result  " +result);

	}

	public static boolean isPalindrome(String s) {
		
		String validInput = "";

		for (char c : s.toCharArray()) {
			if (Character.isDigit(c) || Character.isLetter(c)) {
				validInput += c;
			}
		}
		String lowerCaseString = validInput.toLowerCase();

		int startPointer = 0;
		int endPointer = lowerCaseString.length() - 1;

		while (startPointer <= endPointer) {
			if (lowerCaseString.charAt(startPointer) != lowerCaseString.charAt(endPointer)) {
				return false;
			}
			startPointer++;
			endPointer--;

		}

		return true;

	}

}
