package com.leetcode.shirish.LeetCodePractice.april;

public class DiameterOfATree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TreeNode node = new TreeNode(1);
		node.left = new TreeNode(2);
		node.right = new TreeNode(3);
		node.left.left = new TreeNode(4);
		node.left.right = new TreeNode(5);
	 int result = diameterOfBinaryTree(node);
	 System.out.println("Result "  +result);
		

	}

	static int ans;

	public static int diameterOfBinaryTree(TreeNode root) {
		ans = 1;
		depth(root);
		return ans - 1;
	}

	public static int depth(TreeNode node) {
		if (node == null)
			return 0;
		int L = depth(node.left);
		System.out.println("L : "  +L);
		int R = depth(node.right);
		System.out.println("R : "  +R);
		ans = Math.max(ans, L + R + 1);
		System.out.println("Rans : "  +ans);
		System.out.println("FINAL : "  +(Math.max(L, R) + 1));
		return Math.max(L, R) + 1;
	}

	public static class TreeNode {
		int val;
		TreeNode left;
		TreeNode right;

		TreeNode() {
		}

		TreeNode(int val) {
			this.val = val;
		}

		TreeNode(int val, TreeNode left, TreeNode right) {
			this.val = val;
			this.left = left;
			this.right = right;
		}

	}
}
