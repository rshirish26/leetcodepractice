package com.leetcode.shirish.LeetCodePractice.april;

import java.util.Stack;

/*Given two strings S and T, return if they are equal when both are typed into empty text editors. # means a backspace character.

Note that after backspacing an empty text, the text will continue empty.

Example 1:

Input: S = "ab#c", T = "ad#c"
Output: true
Explanation: Both S and T become "ac".
Example 2:

Input: S = "ab##", T = "c#d#"
Output: true
Explanation: Both S and T become "".
Example 3:

Input: S = "a##c", T = "#a#c"
Output: true
Explanation: Both S and T become "c".
Example 4:

Input: S = "a#c", T = "b"
Output: false
Explanation: S becomes "c" while T becomes "b".
Note:

1 <= S.length <= 200
1 <= T.length <= 200
S and T only contain lowercase letters and '#' characters.
Follow up:

Can you solve it in O(N) time and O(1) space?*/

public class BackSpaceStringCompare {

	public static void main(String[] args) {
	
	  String one = "a##c";
	  String two = "#a#c";

	  boolean result = backspaceCompare(one, two);
	  System.out.println("Result : " +result);
	}

	public static boolean backspaceCompare(String one, String two) {
		
		String buildOne = build(one);
	    String buildTwo = build(two);
		
		return buildOne.equals(buildTwo);

	}

	private static String build(String input) {
		Stack<Character> stack = new Stack<>();
		
		for(char c : input.toCharArray())
		{
			if(c == '#')
			{
				if(!stack.isEmpty())
				{
					stack.pop();
				}
			} else
			{
				stack.add(c);
			}
		}
		
		System.out.println(stack.toString());
		
		return stack.toString();
	}

}
