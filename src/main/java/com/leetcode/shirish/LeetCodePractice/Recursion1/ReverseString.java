package com.leetcode.shirish.LeetCodePractice.Recursion1;

import java.util.Arrays;

/*Write a function that reverses a string. The input string is given as an array of characters char[].

Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.

You may assume all the characters consist of printable ascii characters.

 

Example 1:

Input: ['h','e','l','l','o']
Output: ['o','l','l','e','h']
Example 2:

Input: ['H','a','n','n','a','h']
Output: ['h','a','n','n','a','H']
   Show Hint #1  */
public class ReverseString {

	public static void main(String[] args) {
      char [] input = {'h','e','l','l','o'};
    //  System.out.println(input);
      reverseString(input);
	}

	public static void reverseString(char[] s) {
		reverse(s, 0, s.length-1);
	}

	private static void reverse(char[] s, int left, int right) {
		if(left >= right) return;
		char temp = s[left];
		s[left++] = s[right];
		s[right--] = temp;
		reverse(s, left, right);
		
	}

}
