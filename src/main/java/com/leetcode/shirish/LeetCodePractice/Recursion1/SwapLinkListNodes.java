package com.leetcode.shirish.LeetCodePractice.Recursion1;

/*
Given a linked list, swap every two adjacent nodes and return its head.

You may not modify the values in the list's nodes, only nodes itself may be changed.

 

Example:

Given 1->2->3->4, you should return the list as 2->1->4->3.*/
public class SwapLinkListNodes {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public ListNode swapPairs(ListNode head) {
		
		ListNode dummy = new ListNode(-1);
		dummy.next = head;
		ListNode prevNode = dummy;
		
		while(head !=null || head.next !=null)
		{
			ListNode first= head;
			ListNode second = head.next;
			
			prevNode.next = second;
			first.next = second.next;
			second.next = first;
			
			prevNode = first;
			head = first.next;		
			
		}
		return dummy.next;
	
		
		
		
		
	}
	
	public class ListNode {
		      int val;
		      ListNode next;
		      ListNode(int x) { val = x; }
		  }
	

}
