package com.leetcode.shirish.LeetCodePractice.amazon.recursion;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

/*Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

For example, given n = 3, a solution set is:

[
  "((()))",
  "(()())",
  "(())()",
  "()(())",
  "()()()"
]*/

public class GenerateParenthesis {

	public static void main(String[] args) {
        System.out.println("Output : "+generateParenthesis(3).toString());
	}

	public static List<String> generateParenthesis(int n) {
		List<String> result = new ArrayList<>();
		dfs(result, "", n, n);
		return result;

	}

	private static void dfs(List<String> result, String pattern, int left, int right) {

		if (left > right)
			return;
		if (left == 0 && right == 0) {
			result.add(pattern);
			return;
		}
		if(left > 0)
		{
			dfs(result, pattern+"(", left-1, right);
		}
		if(right > 0)
		{
			dfs(result, pattern+")", left, right-1);
		}

	}

}
