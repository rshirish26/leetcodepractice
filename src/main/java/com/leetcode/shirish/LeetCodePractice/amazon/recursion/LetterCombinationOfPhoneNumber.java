package com.leetcode.shirish.LeetCodePractice.amazon.recursion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*Given a string containing digits from 2-9 inclusive, return all possible letter combinations that the number could represent.

A mapping of digit to letters (just like on the telephone buttons) is given below. Note that 1 does not map to any letters.



Example:

Input: "23"
Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].
Note:

Although the above answer is in lexicographical order, your answer could be in any order you want.*/

public class LetterCombinationOfPhoneNumber {

	static Map<String, String> phone = new HashMap<String, String>() {
		{
			put("2", "abc");
			put("3", "def");
			put("4", "ghi");
			put("5", "jkl");
			put("6", "mno");
			put("7", "pqrs");
			put("8", "tuv");
			put("9", "wxyz");
		}
	};

	static List<String> output = new ArrayList<String>();

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> result = letterCombinations("234");
		System.out.println("Result : " + result.toString());

	}

	public static List<String> letterCombinations(String digits) {
		if (digits.length() != 0) {
			backTracking("", digits);
		}
		return output;
	}

	public static void backTracking(String combination, String digits) {
		if (digits.length() == 0) {
			output.add(combination);
		} else {
			String digit = digits.substring(0, 1);
			String word = phone.get(digit);

			for (int i = 0; i < word.length(); i++) {
				String letter = phone.get(digit).substring(i, i + 1);
				backTracking(combination + letter, digits.substring(1));
			}

		}

	}

}
