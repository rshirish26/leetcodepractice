package com.leetcode.shirish.LeetCodePractice;

import java.util.Arrays;
import java.util.Comparator;

public class ReorderDataInLogFiles {

	public static void main(String args[]) {
		String[] input = { "dig1 8 1 5 1", "let1 art can", "dig2 3 6", "let2 own kit dig", "let3 art zero" };
		reorderLogFiles(input);
	}

	public static String[] reorderLogFiles(String[] logs) {
		
		 class Compare implements Comparator<String> {

			@Override
			public int compare(String one, String two) {
				
				String [] oneArray = one.split(" ", 2);
				String [] twoArray = two.split(" ", 2);
				System.out.println("VALUE " + Arrays.toString(oneArray));
				
				boolean isDigitOne = Character.isDigit(oneArray[1].charAt(0));
				boolean isDigitTwo = Character.isDigit(twoArray[1].charAt(0));
				
				if(!isDigitOne && !isDigitTwo )
				{
					int comp = oneArray[1].compareTo(twoArray[1]);
					if(comp !=0)
					{
						return comp;
					}
					return oneArray[0].compareTo(twoArray[0]);
			     }
				if(!isDigitOne && isDigitTwo )
				{
					return -1;
				} else if(isDigitOne && !isDigitTwo )
				{
					return 1;
				} else {
					return 0;
				}
			 
		 }
		 }
		Arrays.sort(logs, new Compare());
		return logs;
         
    }
}
